package net.animalcontrolmod.mixins;

import net.minecraft.entity.EntityType;
import net.minecraft.entity.SpawnReason;
import net.minecraft.entity.passive.SquidEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.registry.RegistryKey;
import net.minecraft.world.WorldAccess;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.biome.BiomeKeys;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import java.util.Objects;
import java.util.Optional;
import java.util.Random;

@Mixin(SquidEntity.class)
public class SquidEntityMixin {
    //
    // squid should not spawn in rivers, it's just annoying as fuck
    //
    @Inject(at = @At("TAIL"), method = "canSpawn", cancellable = true)
    private static void noSpawnSquidInRivers(EntityType<SquidEntity> type, WorldAccess world, SpawnReason spawnReason, BlockPos pos, Random random, CallbackInfoReturnable<Boolean> info) {
        Optional<RegistryKey<Biome>> optional = world.getBiomeKey(pos);
        if (Objects.equals(optional, Optional.of(BiomeKeys.RIVER))) {
            info.setReturnValue(false);
        } else {
            info.setReturnValue(pos.getY() > 45 && pos.getY() < world.getSeaLevel());
        }
    }
}
